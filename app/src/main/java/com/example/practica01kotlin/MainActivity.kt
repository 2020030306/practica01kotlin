package com.example.practica01kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private var btnPulsar: Button? = null
    private var btnClean: Button? = null
    private var btnClose: Button? = null
    private var txtNombre: EditText? = null
    private var lblSaludar: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // relacionar los objetos
        btnPulsar = findViewById<View>(R.id.btnSaludar) as Button
        txtNombre = findViewById<View>(R.id.txtNombre) as EditText
        lblSaludar = findViewById<View>(R.id.lblSaludar) as TextView
        btnClean = findViewById<View>(R.id.btnLimpiar) as Button
        btnClose = findViewById<View>(R.id.btnCerrar) as Button

        //codificar el evento clic del botón
        btnPulsar!!.setOnClickListener { //Validar
            if (txtNombre!!.text.toString() == "") {
                Toast.makeText(
                    this@MainActivity,
                    "Faltó capturar la información",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val str = "Hola " + txtNombre!!.text.toString() + " ¿Cómo estás?"
                lblSaludar!!.text = str
            }
        }
        btnClean!!.setOnClickListener {
            txtNombre!!.setText("")
            lblSaludar!!.text = ""
        }
        btnClose!!.setOnClickListener { finish() }
    }
}